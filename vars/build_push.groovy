#!/user/bin/env groovy

def call() {
    withCredentials([usernamePassword(credentialsId: 'docker-credential', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        sh "docker build -t gurkan96/myapp:${IMAGE_NAME} ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push gurkan96/myapp:${IMAGE_NAME}"
    }
}