#!/user/bin/env groovy

def call() {
    dir("app") {
        // install all dependencies needed for running tests
        sh "npm install"
        sh "npm run test"
    }
}